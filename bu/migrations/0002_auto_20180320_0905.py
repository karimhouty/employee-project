# Generated by Django 2.0.2 on 2018-03-20 09:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bu', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bu',
            name='manager',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='bu',
            name='name',
            field=models.CharField(max_length=50),
        ),
    ]
