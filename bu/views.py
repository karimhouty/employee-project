from django.contrib import messages
from django.contrib.auth.decorators import login_required, user_passes_test
from django.core.paginator import Paginator
from django.shortcuts import render, redirect, get_object_or_404

from bu.forms import BUForm
from bu.models import BU
from projectSalaried.utils import paginate


# Create your views here.


@login_required  # a shortcut to check if user is login
@user_passes_test(lambda u: u.is_superuser)  # check if the current user is a superuser
# index function to list business unit
def index(request):
    # check if get request has keyword param
    if 'keyword' in request.GET:
        keyword = request.GET['keyword']  # store the keyword param into a variable
        bu_items = BU.objects.filter(name__icontains=keyword)  # like %keyword%
    else:
        bu_items = BU.objects.all()  # get all business units items
        keyword = ""  # set default value for keyword

    paginator = Paginator(bu_items, 5)  # Show 5 business units per page
    pagination = paginate(request, paginator)  # function to get paginated items and items range

    # create a dictionary hold the variables which we send to the template
    context = {
        'items': pagination['page_items'],
        'keyword': keyword,
        'page_range': pagination['page_range'],
    }

    # render the bu/index.html template with the context
    return render(request, 'bu/index.html', context)


@login_required  # a shortcut to check if user is login
@user_passes_test(lambda u: u.is_superuser)  # check if the current user is a superuser
# show the requested business unit by id
def show(request, id):
    # get the business unit object by id if not found raise a http 404 exception
    item = get_object_or_404(BU, pk=id)
    # add the business unit object to the context
    context = {
        'item': item
    }
    # render the 'bu/show.html' with the context variable
    return render(request, 'bu/show.html', context)


@login_required  # a shortcut to check if user is login
@user_passes_test(lambda u: u.is_superuser)  # check if the current user is a superuser
# this function for showing business unit form and also for the submission
def create(request):
    # create BUForm object fill with inputs if the request method is post or None else
    form = BUForm(request.POST or None)
    # check if the forn is valid
    if form.is_valid():
        form.save()  # save the input values into database
        #  display notification message to user
        messages.info(request, 'business unit created')
        # redirect to the business unit index
        return redirect('bu.index')

    # add the business unit form to the context
    context = {
        'form': form
    }

    # render the 'bu/form.html'  template with the context
    return render(request, 'bu/form.html', context)


@login_required  # a shortcut to check if user is login
@user_passes_test(lambda u: u.is_superuser)  # check if the current user is a superuser

# this function for showing business unit update form and also for the submission
def update(request, id):
    # get the business unit object by id if not found raise a http 404 exception
    item = get_object_or_404(BU, pk=id)

    # create BUForm object fill with inputs if the request method is post or None else
    # pass the business unit object to the form
    form = BUForm(request.POST or None, instance=item)
    # check if the business unit form is valid
    if form.is_valid():
        form.save()  # save the form into database
        messages.info(request, 'business unit updated')  # display a message to the user
        return redirect('bu.index')  # redirect to the business unit index

    # add the business unit form to the context
    context = {
        'form': form
    }
    # render the 'bu/form.html' template with the context
    return render(request, 'bu/form.html', context)


@login_required  # a shortcut to check if user is login
@user_passes_test(lambda u: u.is_superuser)  # check if the current user is a superuser
# remove the requested business unit by id
def delete(request, id):
    # get the business unit object by id if not found raise a http 404 exception
    item = get_object_or_404(BU, pk=id)

    # check if the request method is POST
    if request.method == 'POST':
        item.delete()  # remove the selected business unit
        messages.info(request, 'business unit deleted')  # display success message to the user
        return redirect('bu.index')  # redirect to the business unit index

    messages.info(request, 'something wrong')  # display fail message to the user
    return redirect('bu.index')  # redirect to the business unit index
