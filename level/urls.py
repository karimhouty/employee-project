from django.urls import path

from . import views

# map urls with views
urlpatterns = [
    path('', views.index, name='level.index'),
    path('<int:id>', views.show, name='level.show'),
    path('create', views.create, name='level.create'),
    path('update/<int:id>', views.update, name='level.update'),
    path('delete/<int:id>', views.delete, name='level.delete'),
]
