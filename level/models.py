from django.db import models

# Create your models here.
from django.utils import timezone


# create model class for level study
class Level(models.Model):
    # Each attribute of the model represents a database field.
    name = models.CharField(max_length=50)
    description = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(default=timezone.now, blank=True)

    # A Python « magic method » that returns a string representation of any object
    def __str__(self):
        return self.name
