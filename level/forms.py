from django import forms

from level.models import Level


# create a Django Form class to generate level study form
class LevelForm(forms.ModelForm):
    # forms.ModelForm it will build a form, along with the appropriate fields and their attributes, from a Model class.

    class Meta:
        model = Level  # add the model class

        # declare fields to show in our form
        fields = [
            'name',
            'description',
        ]

    def __init__(self, *args, **kwargs):
        super(LevelForm, self).__init__(*args,
                                        **kwargs)  # call the super constructor for the Form class to override it
        self.fields['description'].widget.attrs.update(
            {'class': 'materialize-textarea'})  # add materialize-textarea class to description input
