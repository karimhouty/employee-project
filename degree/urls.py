from django.urls import path

from . import views

# map urls with views
urlpatterns = [
    path('', views.index, name='degree.index'),
    path('<int:id>', views.show, name='degree.show'),
    path('create', views.create, name='degree.create'),
    path('update/<int:id>', views.update, name='degree.update'),
    path('delete/<int:id>', views.delete, name='degree.delete'),
]
