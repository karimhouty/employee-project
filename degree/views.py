from django.contrib import messages
from django.contrib.auth.decorators import login_required, user_passes_test
from django.core.paginator import Paginator
from django.shortcuts import render, redirect, get_object_or_404

from degree.forms import DegreeForm
from degree.models import Degree
from projectSalaried.utils import paginate


# Create your views here.


@login_required  # a shortcut to check if user is login
@user_passes_test(lambda u: u.is_superuser)  # check if the current user is a superuser
# index function to list degree study
def index(request):
    # check if get request has keyword param
    if 'keyword' in request.GET:
        keyword = request.GET['keyword']  # store the keyword param into a variable
        degree_items = Degree.objects.filter(name__icontains=keyword)  # like %keyword%
    else:
        degree_items = Degree.objects.all()  # get all degree study items
        keyword = ""  # set default value for keyword

    paginator = Paginator(degree_items, 5)  # Show 5 degree study per page
    pagination = paginate(request, paginator)  # function to get paginated items and items range

    # create a dictionary hold the variables which we send to the template
    context = {
        'items': pagination['page_items'],
        'keyword': keyword,
        'page_range': pagination['page_range'],
    }

    # render the degree/index.html template with the context
    return render(request, 'degree/index.html', context)


@login_required  # a shortcut to check if user is login
@user_passes_test(lambda u: u.is_superuser)  # check if the current user is a superuser
# show the requested degree by id
def show(request, id):
    # get the degree study object by id if not found raise a http 404 exception
    item = get_object_or_404(Degree, pk=id)
    # add the degree study object to the context
    context = {
        'item': item
    }
    # render the 'degree/show.html' with the context variable
    return render(request, 'degree/show.html', context)


@login_required  # a shortcut to check if user is login
@user_passes_test(lambda u: u.is_superuser)  # check if the current user is a superuser
# this function for showing degree study form and also for the submission
def create(request):
    # create DegreeForm object fill with inputs if the request method is post or None else
    form = DegreeForm(request.POST or None)
    # check if the forn is valid
    if form.is_valid():
        form.save()  # save the input values into database
        #  display notification message to user
        messages.info(request, 'degree study created')
        # redirect to the degree study index
        return redirect('degree.index')

    # add the degree study form to the context
    context = {
        'form': form
    }

    # render the 'degree/form.html'  template with the context
    return render(request, 'degree/form.html', context)


@login_required  # a shortcut to check if user is login
@user_passes_test(lambda u: u.is_superuser)  # check if the current user is a superuser
# this function for showing degree study update form and also for the submission
def update(request, id):
    # get the degree study object by id if not found raise a http 404 exception
    item = get_object_or_404(Degree, pk=id)
    # create DegreeForm object fill with inputs if the request method is post or None else
    form = DegreeForm(request.POST or None, instance=item)
    # check if the degree study form is valid
    if form.is_valid():
        form.save()  # save the form into database
        messages.info(request, 'degree study updated')  # display a message to the user
        return redirect('degree.index')  # redirect to the degree study index

    # add the degree study form to the context
    context = {
        'form': form
    }
    # render the 'degree/form.html' template with the context
    return render(request, 'degree/form.html', context)


@login_required  # a shortcut to check if user is login
@user_passes_test(lambda u: u.is_superuser)  # check if the current user is a superuser
# remove the requested degree by id
def delete(request, id):
    # get the degree study object by id if not found raise a http 404 exception
    item = get_object_or_404(Degree, pk=id)

    # check if the request method is POST
    if request.method == 'POST':
        item.delete()  # remove the selected degree study
        messages.info(request, 'degree study deleted')  # display success message to the user
        return redirect('degree.index')  # redirect to the degree study index

    messages.info(request, 'something wrong')  # display fail message to the user
    return redirect('degree.index')  # redirect to the degree study index
