from django.urls import path

from . import views

# map urls with views
urlpatterns = [
    # Employee Urls Crud
    path('', views.index, name='salaried.index'),
    path('<int:id>', views.show, name='salaried.show'),
    path('create', views.create, name='salaried.create'),
    path('update/<int:id>', views.update, name='salaried.update'),
    path('delete/<int:id>', views.delete, name='salaried.delete'),

    # Document Import Export Urls
    path('import/', views.import_salaried_index, name='salaried.import_salaried_index'),
    path('export/xls/', views.export_salaried_xls, name='salaried.export_xls'),
    path('export/csv/', views.export_salaried_csv, name='salaried.export_csv'),

    # Document Edit Urls
    path('<int:salaried_id>/documents', views.index_document, name='salaried.documents.index'),
    path('<int:salaried_id>/documents/delete/<int:document_id>', views.delete_document,
         name='salaried.documents.delete'),
    path('<int:salaried_id>/documents/create', views.create_document, name='salaried.documents.create'),
    path('<int:salaried_id>/documents/update/<int:document_id>', views.update_document,
         name='salaried.documents.update'),

]
