from django import forms
from django.core.validators import FileExtensionValidator

from .models import Salaried, BU, Level, Degree, Document


# create a Django Form class to generate Employee form
class SalariedForm(forms.ModelForm):
    bu = forms.ModelChoiceField(queryset=BU.objects.all(), empty_label='Select Business unit')
    level_study = forms.ModelChoiceField(queryset=Level.objects.all(), empty_label='Select level study')

    class Meta:
        model = Salaried
        fields = [
            'first_name',
            'last_name',
            'cin_code',
            'birth_day',
            'hire_day',
            'email',
            'cnss_code',
            'bu',
            'experience_years_number',
            'level_study',
            'degrees_study',
        ]

    def __init__(self, *args, **kwargs):
        super(SalariedForm, self).__init__(*args, **kwargs)
        self.fields['bu'].label = "Business unit"  # modify label fot the bu field
        self.fields['birth_day'].widget.attrs.update({'class': 'datepicker'})  # add datepicker class to birth_day field
        self.fields['hire_day'].widget.attrs.update({'class': 'datepicker'})  # add datepicker class to hire_day field

    # def clean_birth_day(self):
    #     date = self.cleaned_data['birth_day']
    #     if date > date.today(): # check if the birth day is greater than the current date
    #         raise forms.ValidationError("The date cannot be in the future!")
    #     return date


# create a Django Form class to generate Document form
class DocumentForm(forms.ModelForm):
    class Meta:
        model = Document
        fields = [
            'label',
            'description',
            'attached_piece'
        ]

    def __init__(self, *args, **kwargs):
        super(DocumentForm, self).__init__(*args, **kwargs)
        # self.fields['description'].widget.attrs.update({'class': 'materialize-textarea'})
        self.fields['attached_piece'].widget.attrs.update({'class': 'file-input'})
        # self.fields['attached_piece'].label = "CV"
        self.fields['description'].widget.attrs.update({'class': 'materialize-textarea'})
        self.fields['attached_piece'].label = "attached piece"


# create a Django Form class to generate Export Form
class ExportForm(forms.Form):
    attached_piece = forms.FileField(widget=forms.FileInput(
        attrs={
            'accept': '.xls,.csv',
            'name': 'attached_piece',
            'class': 'file-input'
        },
    ),
        validators=[FileExtensionValidator(['xls', 'csv'])]  # valid extensions
    )

    fields = [
        'attached_piece'
    ]

