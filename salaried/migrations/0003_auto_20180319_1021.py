# Generated by Django 2.0.2 on 2018-03-19 10:21

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('degree', '0001_initial'),
        ('level', '0001_initial'),
        ('salaried', '0002_auto_20180310_0806'),
    ]

    operations = [
        migrations.AddField(
            model_name='salaried',
            name='degrees_study',
            field=models.ManyToManyField(to='degree.Degree'),
        ),
        migrations.AddField(
            model_name='salaried',
            name='experience_years_number',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='salaried',
            name='level_study',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='level.Level'),
        ),
    ]
