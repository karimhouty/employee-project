# Generated by Django 2.0.2 on 2018-03-20 09:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('salaried', '0003_auto_20180319_1021'),
    ]

    operations = [
        migrations.AlterField(
            model_name='salaried',
            name='experience_years_number',
            field=models.IntegerField(default=0),
        ),
    ]
